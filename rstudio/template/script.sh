#!/usr/bin/env bash

#Command Tracing
#set -x

# Benchmark info
echo "TIMING - Starting main script at: $(date)"

# Clean up the modules in working environment
module -q reset

# Prepare environment based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	RSTUDIO_SERVER_IMAGE="$OOD_SIF"
else
	# Load Initial Modules
	# shellcheck disable=SC2086
	module load $OOD_MODULES

	# Report Modules
	echo ""
	echo "The Following Modules Are Loaded:"
	module --terse list
	echo ""

	# Get SIF Image from Module
	if [ -n "$SIF" ] && [ -f "$SIF" ]; then
		RSTUDIO_SERVER_IMAGE="$SIF"
	elif command -v R.sif &>/dev/null; then
		RSTUDIO_SERVER_IMAGE="$(command -v R.sif)"
	else
		echo "Error: Cannot determine SIF Image from module environment! Stopping!!"
		exit 22
	fi
fi

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$RSTUDIO_SERVER_IMAGE"
echo ""

# Validate Container
if ! apptainer exec "$RSTUDIO_SERVER_IMAGE" bash -c "command -v rserver &>/dev/null"; then
	echo "Error: 'rserver' is missing from the PATH inside the provided container! Stopping!!"
	exit 33
fi
if ! apptainer exec "$RSTUDIO_SERVER_IMAGE" bash -c "command -v R &>/dev/null"; then
	echo "Error: 'R' is missing from the PATH inside the provided container! Stopping!!"
	exit 34
fi

# Create Config File
cat <<-EOF >./rserver.conf
	server-user=$USER
	www-port=${PORT}
	auth-none=0
	auth-pam-helper-path=${PWD}/bin/auth
	auth-encrypt-password=0
	auth-timeout-minutes=0
	server-app-armor-enabled=0
EOF

# Create Session Config File
cat <<-EOF >./rsession.conf
	session-timeout-minutes=0
	session-save-action-default=no
EOF

# Create Log Config File (debug, info, warn, error)
cat <<-EOF >./logging.conf
	[*]
	log-level=info
	logger-type=file
	log-dir=$PWD/sessions
EOF

# Generate R Environment Site File
apptainer exec --bind="/tmp,$(pwd)" "$RSTUDIO_SERVER_IMAGE" bash ./genenv.sh
renv_path="$(cat ./Renviron.path)"

# Create RStudio Writable Directories and Helper Files
mkdir -p "$TMPDIR/rstudio-server/var/lib"
mkdir -p "$TMPDIR/rstudio-server/var/run"
uuidgen >"$TMPDIR/rstudio-server/secure-cookie-key"
chmod 600 "$TMPDIR/rstudio-server/secure-cookie-key"

# Setup Bind Mounts
BPath="/tmp"
BPath+=",$TMPDIR/rstudio-server/var/lib:/var/lib/rstudio-server"
BPath+=",$TMPDIR/rstudio-server/var/run:/var/run/rstudio-server"
BPath+=",logging.conf:/etc/rstudio/logging.conf"
BPath+=",rsession.conf:/etc/rstudio/rsession.conf"
BPath+=",rserver.conf:/etc/rstudio/rserver.conf"
BPath+=",Renviron.site:$renv_path"
BPath+=",sessions:${HOME}/.local/share/rstudio/sessions"

# Launch the RStudio Server
echo "TIMING - Starting RStudio Server at: $(date)"
if [ -n "$CUDA_VISIBLE_DEVICES" ]; then
	apptainer exec --nv --bind="$BPath" "$RSTUDIO_SERVER_IMAGE" rserver
else
	apptainer exec --bind="$BPath" "$RSTUDIO_SERVER_IMAGE" rserver
fi
