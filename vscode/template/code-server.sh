#!/usr/bin/env bash

# Set Path To 'code-server' Binary
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]] \
	&& apptainer exec "$OOD_SIF" bash -c "command -v code-server &>/dev/null"
then
	OOD_CODE_SERVER="$(apptainer exec "$OOD_SIF" bash -c "command -v code-server")"
else
	module load spack/2024a code-server/4.95.3-2024a-gcc_8.5.0-d7itjg4
	OOD_CODE_SERVER="$(which --skip-alias --skip-functions code-server)"
	module -q reset
fi
export OOD_CODE_SERVER

# Set Fuction Based On Determined PATH
function code-server { ood_launch "$OOD_CODE_SERVER" "$@" ; }
export -f code-server

# Setup dirs for code-server permanant data
CODE_SERVER_DATAROOT="$HOME/.local/share/code-server"
mkdir -p "$CODE_SERVER_DATAROOT/extensions"

# Handle User Env Script
source "${OOD_STAGED_ROOT}/user-env.sh"

# Start Code Server
code-server \
	--auth="password" \
	--bind-addr="0.0.0.0:${PORT}" \
	--disable-telemetry \
	--disable-update-check \
	--extensions-dir="$CODE_SERVER_DATAROOT/extensions" \
	--user-data-dir="$CODE_SERVER_DATAROOT" \
	--ignore-last-opened \
	"$OOD_VSCODE_WORKSPACE"
