#!/usr/bin/env bash

# Set Path To 'code' Binary
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]] \
	&& apptainer exec "$OOD_SIF" bash -c "command -v code &>/dev/null"
then
	OOD_CODE_SERVER="$(apptainer exec "$OOD_SIF" bash -c "command -v code")"
else
	module load custom/2024a ms-code-cli/1.96.1
	OOD_CODE_SERVER="$(which --skip-alias --skip-functions code)"
	module -q reset
fi
export OOD_CODE_SERVER

# Set Fuction Based On Determined PATH
function code { ood_launch "$OOD_CODE_SERVER" "$@" ; }
export -f code

# Create Password File
PFILE="${OOD_STAGED_ROOT}/password.txt"
echo "$PASSWORD" > "$PFILE"

# Handle User Env Script
source "${OOD_STAGED_ROOT}/user-env.sh"

# Start Code Server
code serve-web \
	--host="0.0.0.0" \
	--port="$PORT" \
	--connection-token-file="$PFILE" \
	--accept-server-license-terms \
	--server-base-path="/node/$HOST/$PORT/"
