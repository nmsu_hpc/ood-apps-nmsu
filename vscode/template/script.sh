#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# Create Function to handle SIF vs Bare Execution
function ood_launch {
	if command -v apptainer &>/dev/null \
		&& [ -z "$APPTAINER_COMMAND" ] \
		&& [ ! -d '/.singularity.d' ] \
		&& [ ! -d '/.apptainer.d' ] \
		&& [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]] \
		&& [ -n "$OOD_SIF" ] \
		&& [ -f "$OOD_SIF" ]
	then
		if [ -n "$CUDA_VISIBLE_DEVICES" ]; then
			apptainer exec --nv "$OOD_SIF" "$@"
		else
			apptainer exec "$OOD_SIF" "$@"
		fi
	else
		"$@"
	fi
}
export -f ood_launch

# Create Function for Slurm Cleanup
function slurm_env_clean {
	eval "$(printenv | grep -i ^slurm | sed 's/^SLURM/export OOD_SLURM/')"
	# shellcheck disable=SC2046
	unset $(compgen -v 'SLURM')
	echo "All environment variables starting with 'SLURM' have been renamed to be prefixed with 'OOD_'."
	printenv | grep -i '^OOD_SLURM'
}
export -f slurm_env_clean

# Ensure Compatible Workspace
if [ ! -d "$OOD_VSCODE_WORKSPACE" ]; then
	if [ ! -f "$OOD_VSCODE_WORKSPACE" ] \
		|| ! grep -Eq '\.code-workspace$' <<< "$OOD_VSCODE_WORKSPACE"
	then
		echo "Error: '$OOD_VSCODE_WORKSPACE' is invalid! Using Default Workspace File!!"
		OOD_VSCODE_WORKSPACE="${OOD_STAGED_ROOT}/job.code-workspace"
	fi
fi

# Set Working Directory
if [ -d "$OOD_VSCODE_WORKSPACE" ]; then
	cd "$OOD_VSCODE_WORKSPACE" || true
fi

# Start VSCode
echo -e "\nStarting Code Server:"
if [ "$OOD_VSCODE_VENDOR" == "coder" ]; then
	bash "${OOD_STAGED_ROOT}/code-server.sh"
elif [ "$OOD_VSCODE_VENDOR" == "ms" ]; then
	bash "${OOD_STAGED_ROOT}/ms-code-cli.sh"
else
	echo "Invalid Code Server Vendor Specified! Stopping!!"
	exit 10
fi
