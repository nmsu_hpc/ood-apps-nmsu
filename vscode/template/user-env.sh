#!/usr/bin/env bash

# Source User's Environment Setup Secript
if [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Starting: Sourcing User Environment Setup Script"
	# shellcheck disable=SC1090
	source "$OOD_ENV_SETUP_SCRIPT"
	echo "Finished: Sourcing User Environment Setup Script"
elif [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ ! -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Error: User environment setup script '$OOD_ENV_SETUP_SCRIPT' specified but does not exist! Skipping!!"
fi
