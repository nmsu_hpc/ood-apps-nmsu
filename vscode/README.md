# OOD Interactive VSCode App

This is an Open Ondemand app designed to run a web version of VSCode.

Both software modules and SIF containers are supported by this app.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [Apptainer](https://apptainer.org/)
- [Microsoft VSCode Standalone Code CLI](https://code.visualstudio.com/download)
- [Coder Code-Server](https://github.com/coder/code-server)

When using containers if the VSCode vendor binary exists in the default `$PATH` the version from the container will be used. If not then the default binaries, from outside the container, will be used by apptainer. This assumes your software directories are mounted in by default via `/etc/apptainer/apptainer.conf` on the host.

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
  - filepicker entries for favorite folders.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `template/script.sh`
  - Calls to apptainer assumes that the nodes apptainer.conf will specify any cluster specific mounts such as home, scratch, and project directories.
- `template/ms-code-cli.sh`
  - Update modules used to find the default version of `code`.
- `template/code-server.sh`
  - Update modules used to find the default version of `code-server`.
