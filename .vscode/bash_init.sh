#!/usr/bin/env bash

# Source defaults
if [ -f "/etc/profile" ]; then
	source "/etc/profile"
fi

if [ -f "$HOME/.profile" ]; then
	source "$HOME/.profile"
fi

# Set Vars
PRJ_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" | awk 'NF{NF-=1}1' FS='/' OFS='/')"
CACHE_DIR="${PRJ_DIR}/cache"
SCRIPTS_DIR="${PRJ_DIR}/.scripts"
export PRJ_DIR CACHE_DIR SCRIPTS_DIR

# Source Project
if [ -f "$SCRIPTS_DIR/project.sh" ]; then
	source "$SCRIPTS_DIR/project.sh"
	project
fi

# Source Git Completions
if [ -f "/usr/share/bash-completion/completions/git" ]; then
	source "/usr/share/bash-completion/completions/git"
fi
