# Batch Connect Shell

This app will make a remote persistant shell available over http (https provided by ondemand reverse proxy).

To do this we use a tool called [ttyd](https://tsl0922.github.io/ttyd/).  This is a very small binary that acts as a web server and serves a [xterm.js](https://xtermjs.org/) terminal.

One issue with ttyd is that it does not persist sessions.  So disconnects/reconnects spawn a brand new terminal while the old one is killed. To work around this we use a terminal multiplexor such as tmux, or screen, to provide persistance.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [ttyd](https://tsl0922.github.io/ttyd/)
- [tmux](https://github.com/tmux/tmux/wiki)
- [screen](https://www.gnu.org/software/screen/)

## Known Issues

- A bug in ttyd version 1.6.3 prevents reverse proxies (-b or --base-path) were the url is greater then 20 characters.
  - This is fixed in the master branch so use that until a new version of ttyd is tagged/released.
- tmux configuration files are not backwards compatible and have a tendency to break between versions.
  - The include tmux.conf is based on tmux 3.1b.
- Dynamic font/zoom resizing keybinds are intercepted by the web browser causing display issues.
  - Ctrl+Plus, Ctrl+Minus, and Ctrl+0
  - The browser zoom is adjusted at the same time the terminal tries to adjust which causes the terminal to redraw/reset its column/row count.
  - The workaround is to configure the font size at job submittion.
- Copy on select does not work when tmux mouse mode is enabled.
  - Selection works but the copy to clipboard does not.
  - Workaround is to use the 'shift' modifier while selecting text.
- The only way to stop the job is to delete the interactive job card.
  - Killing the tmux session does not stop ttyd.  On next connection a new tmux session will get started.

## Security

- ttyd password is visible in /proc due to the password being passed directly in the call.
  - To mitigate this impliment the 'hidepid' mount option to '/proc' in order to prevent users from seeing processes that do not belong to them.
    - [RHEL 7+ reccomends against using hidepid](https://access.redhat.com/solutions/6704531) so another solution is likely needed.
- The url when clicking on the connect button includes a username and password.
  - Chrome/Firefox hides the username/password in the url so it should be safe for screen sharing.
  - Chrome/Firefox may have a popup that notifies the users of basic auth username and asks for 'ok'.
    - This doesn't expose password and the user simply has to hit 'ok'.

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `template/tmux.sh`, `template/screen.sh`, and `template/ttyd.sh`
  - Change edit or remove the module load and module reset commands as needed.
