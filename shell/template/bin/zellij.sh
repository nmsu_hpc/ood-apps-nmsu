#!/usr/bin/env bash

# get full path to latest zellij binary
module -q load spack/2024a zellij/0.41.2-2024a-gcc_8.5.0-theeaea
ZELLIJ_PATH="$(which --skip-alias --skip-functions zellij)"
export ZELLIJ_PATH
module -q reset

# select a config file
if [ -f "${HOME}/.config/zellij/config.kdl" ]; then
	ZELLIJ_CONFIG_DIR="${HOME}/.config/zellij"
	ZELLIJ_CONFIG_FILE="$ZELLIJ_CONFIG_DIR/config.kdl"
else
	ZELLIJ_CONFIG_DIR="$OOD_STAGED_ROOT/config/zellij"
	ZELLIJ_CONFIG_FILE="$ZELLIJ_CONFIG_DIR/config.kdl"
fi
export ZELLIJ_CONFIG_DIR ZELLIJ_CONFIG_FILE

# set additional zellij env vars
ZELLIJ_SESSION="J${SLURM_JOB_ID}"
export ZELLIJ_SOCKET ZELLIJ_SESSION

# override 'zellij' to use the desired path
function zellij {
	"$ZELLIJ_PATH" "$@"
}
export -f zellij

# run app
zellij --config-dir "$ZELLIJ_CONFIG_DIR" attach "$ZELLIJ_SESSION" --create options --on-force-close detach
