#!/usr/bin/env bash

# get full path to latest tmux binary
module -q load spack/2024a tmux/3.4-2024a-gcc_8.5.0-3wkc7lr
TMUX_PATH="$(which --skip-alias --skip-functions tmux)"
export TMUX_PATH
module -q reset

# select a config file
if [ -f "${HOME}/.config/tmux/tmux.conf" ]; then
	TMUX_CONFIG_PATH="${HOME}/.config/tmux/tmux.conf"
elif [ -f "${HOME}/.tmux.conf" ]; then
	TMUX_CONFIG_PATH="${HOME}/.tmux.conf"
else
	TMUX_CONFIG_PATH="$OOD_STAGED_ROOT/config/tmux.conf"
fi
export TMUX_CONFIG_PATH

# set additional tmux env vars
TMUX_SOCKET="J${SLURM_JOB_ID}.sock"
TMUX_SESSION="J${SLURM_JOB_ID}"
export TMUX_SOCKET TMUX_SESSION

# override 'tmux' to use the desired path
function tmux {
	"$TMUX_PATH" "$@"
}
export -f tmux

# run app
tmux -f "$TMUX_CONFIG_PATH" -L "$TMUX_SOCKET" new -A -s "$TMUX_SESSION"
