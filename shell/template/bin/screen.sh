#!/usr/bin/env bash

# get full path to latest screen binary
#module -q load spack/2024a screen
SCREEN_PATH="$(which --skip-alias --skip-functions screen)"
export SCREEN_PATH
#module -q reset

# setup config file
if [ ! -f "${HOME}/.screenrc" ]; then
	cp -f "$OOD_STAGED_ROOT/.screenrc" "${HOME}/.screenrc"
fi

# select a config file
if [ -f "${HOME}/.config/screen/screen.rc" ]; then
	SCREEN_CONFIG_PATH="${HOME}/.config/screen/screen.rc"
elif [ -f "${HOME}/.screenrc" ]; then
	SCREEN_CONFIG_PATH="${HOME}/.screenrc"
else
	SCREEN_CONFIG_PATH="$OOD_STAGED_ROOT/config/screen.rc"
fi
export SCREEN_CONFIG_PATH

# set additional screen env vars
SCREEN_SESSION="J${SLURM_JOB_ID}"
export SCREEN_SESSION

# setup function to use discovered path
function screen {
	"$SCREEN_PATH" "$@"
}
export -f screen

# run app
if screen -ls | grep -q "\.$SCREEN_SESSION\b"; then
	screen -d -r "$SCREEN_SESSION" -c "$SCREEN_CONFIG_PATH"
else  # create the screen with name $SCREEN_SESSION
	screen -S "$SCREEN_SESSION" -c "$SCREEN_CONFIG_PATH"
fi
