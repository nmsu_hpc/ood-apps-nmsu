#!/usr/bin/env bash

# benchmark info
echo "TIMING - Starting main script at: $(date)"

# reset module environment to default
module -q reset

# set the TERM
export TERM="xterm-256color"

# source ttyd function setup
source "$OOD_STAGED_ROOT/ttyd.sh"

# Set launch script for ttyd
if [ "$OOD_T_MUX" == 'zellij' ]; then
	OOD_TTYD_LAUNCH_SCRIPT="$OOD_STAGED_ROOT/bin/zellij.sh"
elif [ "$OOD_T_MUX" == 'tmux' ]; then
	OOD_TTYD_LAUNCH_SCRIPT="$OOD_STAGED_ROOT/bin/tmux.sh"
elif [ "$OOD_T_MUX" == 'screen' ]; then
	OOD_TTYD_LAUNCH_SCRIPT="$OOD_STAGED_ROOT/bin/screen.sh"
else
	echo "'$OOD_T_MUX' is not an expected multiplexer value! Stopping!!"
	exit 8
fi

# set working directory
cd "$HOME" || exit 7

# benchmark info
echo "TIMING - Starting ttyd at: $(date)"

# launch ttyd
ttyd \
	-p "${PORT}" \
	-b "/node/${HOST}/${PORT}" \
	-t "fontSize=${OOD_FONT_SIZE}" \
	-t 'cursorStyle=bar' \
	-t 'cursorBlink=true' \
	-t 'theme={ "background": "#282A36", "black": "#21222C", "blue": "#BD93F9", "brightBlack": "#6272A4", "brightBlue": "#D6ACFF", "brightCyan": "#A4FFFF", "brightGreen": "#69FF94", "brightMagenta": "#FF92DF", "brightRed": "#FF6E6E", "brightWhite": "#FFFFFF", "brightYellow": "#FFFFA5", "cursor": "#F8F8F2", "cursorAccent": "#44475A", "cyan": "#8BE9FD", "foreground": "#F8F8F2", "green": "#50FA7B", "magenta": "#FF79C6", "red": "#FF5555", "selectionBackground": "#44475A", "selectionForeground": "#F8F8F2", "selectionInactiveBackground": "#4e5a82", "white": "#F8F8F2", "yellow": "#F1FA8C" }' \
	-c "ttyd:${PASSWORD}" \
	--writable \
	"$OOD_TTYD_LAUNCH_SCRIPT"
