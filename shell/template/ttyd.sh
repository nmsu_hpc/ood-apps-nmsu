#!/usr/bin/env bash

# load the required modules
module -q load spack/2024a ttyd/main-2024a-gcc_8.5.0-tnvff3d

# set environment variables
TTYD_PATH="$(which --skip-alias --skip-functions ttyd)"
export TTYD_PATH

# unload and cleanup modules
module -q reset

# setup function to use discovered path
function ttyd {
	"$TTYD_PATH" "$@"
}
export -f ttyd
