# frozen_string_literal: true

require 'open3'

# query slurm for user accessible partitions
slurm_partitions_script = 'sinfo -h --format="%P"'
$slurm_partitions_output, $slurm_partitions_status = Open3.capture2('bash', stdin_data: slurm_partitions_script)

# report results
puts '## Start: Slurm Partitions ##'
if $slurm_partitions_status.success?
  puts $slurm_partitions_output
else
  puts 'Slurm Partition Query Error!'
end
puts '## End Slurm Partitions'
