# Ruby Initializers

This folder contains ruby files that need to setup, by hand, for this set of applications to work. The assumptions is that these apps, and initializers, are being used with the Slurm schedular.

## Dashboard

To setup the dashboard initializers copy the ruby files in `./initializers/dashboard/*.rb` to `/etc/ood/config/apps/dashboard/initializers/`.  Any running puns will need to be restarted for the changes to take affect.
