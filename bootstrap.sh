#!/usr/bin/env bash

# Don't Allow Script To Be Sourced
if [ ! "${BASH_SOURCE[0]}" -ef "$0" ]; then
	echo "This script is meant to be executed and not sourced! Stopping!!"
	return 10
fi

# Stop On Error
set -eo pipefail

# Get Script Directory
Script_Dir="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Set Working Directory
cd "$Script_Dir" || exit 1

# Handle User Input
cmd="$1"
if [ "$cmd" == "shell" ]; then
	cd "$Script_Dir" || exit 1
	source "$Script_Dir/.scripts/bootstrap/common.sh"
	bash -c "export PRJ_DIR='$Script_Dir'; source '$Script_Dir/.scripts/project.sh'; project"
	PRJ_RUN /bin/bash -l

elif [ "$cmd" == "vscode" ]; then
	cd "$Script_Dir" || exit 1
	"$Script_Dir/.scripts/bootstrap/vscode.sh"

elif [ "$cmd" == "build" ]; then
	cd "$Script_Dir" || exit 1
	"$Script_Dir/.scripts/bootstrap/build.sh"

elif [ "$cmd" == "link-dev" ]; then
	cd "$Script_Dir" || exit 1
	"$Script_Dir/.scripts/bootstrap/link-dev.sh"

elif [ "$cmd" == "link-prod" ]; then
	cd "$Script_Dir" || exit 1
	"$Script_Dir/.scripts/bootstrap/link-prod.sh"

else
	echo "Helper commands/shortcuts for bootstrapping this project."
	echo ""
	echo "Example:"
	echo "./boostrap <command>"
	echo
	echo "Commands:"
	echo "help         This help menu."
	echo ""
	echo "shell        Enter a bash shell inside the dev container."
	echo "vscode       Run a code-server instance inside the dev container."
	echo "build        Build the dev container and exit."
	echo ""
	echo "link-dev     Link Apps to OnDemand Dev Sandbox."
	echo "link-prod    Link Apps to OnDemand Global Prod Apps (run as 'root')."
	echo ""
fi
