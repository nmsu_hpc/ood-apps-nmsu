# Open OnDemand Application Repository

This repository contains all of New Mexico State University's (NMSU) Open OnDemand (OOD) applications. OOD apps in this repo all share code using the templates directory (combinations of relative symlinks and Ruby ERB imports).

## Installation

These application are tailored to the NMSU Discovery Slurm Cluster and will require modificaitons for use at other institutions.  See the per app README.md files for required modifications.

- Ensure OOD 3.0.x is setup and this repo is cloned to either the OOD server, or home directory (dev sandboxes).
- Ensure that the app/Ruby initializers are installed on the OOD server.
  - See the initializers folder [README.md](initializers/README.md) for instructions.
- Link the project's OOD application folders to the expected OOD app folder.
  - Production: `./bootstrap link-prod`
    - Run as `root` user.
  - Development Sandbox: `./bootstrap link-dev`
    - Run as yourself.

## Development

For convenience, apptainer is used to provided a containerized development environment with the required libraries and tools for error checking and other tasks. A web version of VSCode called code-server is reccommended but CLI editors such as vim/emacs/neovim are also included.

- Ensure you have apptainer installed
- Build container, ruby gems, and pre-commit hooks.
  - `./bootstrap build`
- Run Container
  - code-server based development: `./bootstrap vscode`
  - CLI editor based development: `./bootstrap shell`

The container, built by this repo, can also be used with the OOD code-server app by:

1. Build container: `./bootstrap build`
2. In OOD code-server app:
   1. Select this cloned repo for the Working Directory
   2. Select `./cache/container.sif` as the container image to use.

### Project Command

Once in the dev container a terminal command called `project` has been included to help with linting (pre-commit), git, and other common operations. Run `project` and any of its subcommands, without arguments, to see help text. `project` only has 1 layer of submcommands.

### Slurm Partitions

Apps in this repo get their Slurm partition information from 2 sources. First, is the ruby dashboard initializer that queries Slurm for a list of allowed partitions that a user can use. Second, are the json files, such as `partitions.json`, in the `templates` folder. In order for a partition to show in the app partition/queue dropdown, it must be found in both sources.

The json files are also used to set OOD app attributes for min/max, data hides, and other special OOD dynamic options on a per partition basis. This means that depending on the selected partition we can, as an example, set the max wall time to 1 day for one partition and 7 days for another.

To add/remove partitions, or change OOD dynamic options, modify `templates/partitions.json`.

### New Application

To add a new OOD app do the following:

1. create folder at base of the repository and set as you working directory.
   1. `mkdir example-app`
   2. `cd example-app`
2. Create Required Symlinks
   1. `ln -rs ../templates shared`
   2. `ln -rs ../templates/form.js form.js`
3. Setup required files and README.md to complete the OOD app.
   1. See other apps in this repository for how to use shared templates for the following files:
      1. `form.yml.erb`
      2. `submit.yml.erb`
4. Add app folder name to `apps.txt` and sort the file alphabetically.
