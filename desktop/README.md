# Batch Connect - Desktop

This is an Open Ondemand app designed to run a XFCE desktop, over VNC, and displayed to the user via a web client. The session will stop if the user logs out of XFCE.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [XFCE Desktop](https://www.xfce.org/)
- [TurboVNC](https://www.turbovnc.org/)
  - RPM available through OOD official repos.
- [Websockify](https://github.com/novnc/websockify)
  - RPM available through OOD official repos.

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.

## Container Submission Environment: Required Packages
- `TurboVNC` and `Websockify`:
  - TurboVNC and Websockify need to be installed inside the container. This is important as all processes will run inside the container and not on the host.
- `Apptainer`
  - In order to use other containers within the containerized desktop (nested container), such as MatLab, Apptainer needs to be installed.

Please refer to the *Example Singularity Build File* given below for details on how to do it.

## Example Singularity Build File
my_desktop.def
```txt
Bootstrap: docker
From: docker.io/rockylinux/rockylinux:8

%environment
  PATH=/opt/TurboVNC/bin:$PATH
  LANGUAGE="en_US.UTF-8"
  LC_ALL="en_US.UTF-8"
  LANG="en_US.UTF-8"

%post
    # Install EPEL repository for additional packages
    dnf install -y epel-release
    dnf groupinstall -y xfce
    dnf install -y python3-pip xorg-x11-xauth
    pip3 install ts
    
    # Install Websockify
    dnf install -y https://yum.osc.edu/ondemand/latest/compute/el8Server/x86_64/python3-websockify-0.10.0-1.el8.noarch.rpm
    
    # Install TurboVNC
    dnf install -y https://yum.osc.edu/ondemand/latest/compute/el8Server/x86_64/turbovnc-2.2.5-1.el8.x86_64.rpm

    # Install Apptainer
    dnf install -y https://github.com/apptainer/apptainer/releases/download/v1.3.1/apptainer-1.3.1-1.x86_64.rpm
    # or "dnf install -y apptainer"

    dnf clean all
    chown root:root /opt/TurboVNC/etc/turbovncserver-security.conf
    rm -rf /var/cache/dnf/*

%environment
    export PATH=$PATH:/usr/local/bin
```
