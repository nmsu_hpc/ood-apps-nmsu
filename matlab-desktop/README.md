# Batch Connect - MATLAB

This is an Open Ondemand app designed to run a XFCE desktop, over VNC, and displayed to the user via a web client. Once the desktop is running, a MATLAB GUI will be automatically launched. If the 'backfill' partition is selected, then code will be automatically started inside the GUI. The session will stop if either MATLAB is killed or if the user logs out of XFCE. This is based on the [desktop](../desktop/README.md) with MATLAB thrown on top.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [Apptainer](https://apptainer.org/)
- [MATLAB](https://www.mathworks.com/products/matlab.html)
  - This can be containerized or provided via Lmod modules.
- [XFCE Desktop](https://www.xfce.org/)
- [TurboVNC](https://www.turbovnc.org/)
  - RPM available through OOD official repos.
- [Websockify](https://github.com/novnc/websockify)
  - RPM available through OOD official repos.

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
  - `version:` module entries.
    - It's expected that the module will provide a `$SIF` environment variable.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `template/sif.sh`
  - Calls to apptainer assumes that the nodes apptainer.conf will specify any cluster specific mounts such as home, scratch, and project directories.
