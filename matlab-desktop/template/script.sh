#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# Setup Cleanup Trap
PROC=$$
cleanup() {
	local pids
	pids=$(jobs -pr)
	# shellcheck disable=SC2086
	[ -n "$pids" ] && kill $pids
	exit 0
}
# shellcheck disable=SC2172,SC2102
trap "cleanup" INT QUIT TERM EXIT 10

# CD Into Home Directory
cd "$HOME" || true

# fix dbus issues with RHEL 8
unset DBUS_SESSION_BUS_ADDRESS

# Start Desktop
(
	echo "Launching Deskop '$OOD_DESKTOP' ..."
	# shellcheck disable=SC1090
	source "${OOD_STAGED_ROOT}/desktops/${OOD_DESKTOP}.sh" "${OOD_STAGED_ROOT}/desktops/configs"
	echo "Deskop '$OOD_DESKTOP' endec ..."
	kill -10 $PROC
) &

# Prepare 'matlab' based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/sif.sh"
else
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/modules.sh"
fi

# Source User's Environment Setup Secript
if [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Starting: Sourcing User Environment Setup Script"
	# shellcheck disable=SC1090
	source "$OOD_ENV_SETUP_SCRIPT"
	echo "Finished: Sourcing User Environment Setup Script"
elif [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ ! -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Error: User environment setup script '$OOD_ENV_SETUP_SCRIPT' specified but does not exist! Skipping!!"
fi

# Validate Matlab is in PATH or Function
if ! which matlab &>/dev/null; then
	"Error: 'matlab' is not in PATH or defined as a function! Stopping!!"
	exit 9
fi

# Start Matlab
(
	cd "$OOD_WD" || true
	matlab -desktop -r "$OOD_MCOMMAND"
	kill -10 $PROC
) &

# Pause until both matlab and xfce are stopped
wait
