#!/usr/bin/env bash

# Setup 'project' completions
if command -v _project &>/dev/null && command -v project &>/dev/null ; then
	complete -F _project project
fi

# Source Git Completions
if [ -f "/usr/share/bash-completion/completions/git" ]; then
	source "/usr/share/bash-completion/completions/git"
fi
