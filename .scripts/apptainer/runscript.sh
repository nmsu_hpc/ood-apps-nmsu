#!/usr/bin/env bash

# Setup XDG_RUNTIME_DIR
if [ -z "$XDG_RUNTIME_DIR" ] \
	|| [ ! -d "$XDG_RUNTIME_DIR" ] \
	|| [ ! -w "$XDG_RUNTIME_DIR" ]
then
	XDG_RUNTIME_DIR="$(mktemp -d "/dev/shm/${UID}-run-XXXXXXX")"
	export XDG_RUNTIME_DIR
elif [ "$XDG_RUNTIME_DIR" == "/run/user/${UID}" ]; then
	XDG_RUNTIME_DIR="$(mktemp -d "/run/user/${UID}/${UID}-run-XXXXXXX")"
	export XDG_RUNTIME_DIR
fi

# Setup TMPDIR
if [ -z "$TMPDIR" ] \
	|| [ ! -d "$TMPDIR" ] \
	|| [ ! -w "$TMPDIR" ]
then
	TMPDIR="$(mktemp -d "/tmp/${UID}-tmpdir-XXXXXXX")"
	export TMPDIR
fi

if [ ! -e "$SSH_AUTH_SOCK" ] \
	|| ! nc -zU "$SSH_AUTH_SOCK"
then
	# Start SSH Agent
	SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.sock"
	SSH_AGENT_PID_FILE="$XDG_RUNTIME_DIR/ssh-agent.pid"
	export SSH_AUTH_SOCK SSH_AGENT_PID_FILE
	eval "$(ssh-agent -s -t 9h -a "${SSH_AUTH_SOCK}")" &>/dev/null
	echo "$SSH_AGENT_PID" > "$SSH_AGENT_PID_FILE"
	SSH_AGENT_PID="$(cat "$SSH_AGENT_PID_FILE")"
	export SSH_AGENT_PID
fi

# Start GPG Agent
if ! gpg-connect-agent /bye &>/dev/null; then
	GNUPGHOME="$XDG_RUNTIME_DIR"
	export GNUPGHOME
	if [ ! -f "$GNUPGHOME/gpg-agent.conf" ]; then
		cat <<- EOF > "$GNUPGHOME/gpg-agent.conf"
			allow-loopback-pinentry
			pinentry-program /usr/bin/pinentry-tty
			default-cache-ttl 32400
			max-cache-ttl 32400
		EOF
		gpg-connect-agent updatestartuptty /bye &>/dev/null
	fi
fi
GPG_TTY=$(tty)
export GPG_TTY

# Source 'project' function
if [ -n "$PRJ_DIR" ] && [ -d "$PRJ_DIR" ]; then
	cd "$PRJ_DIR" || true
	source "${PRJ_DIR}/.scripts/project.sh"
fi

# Execute User Command
exec "$@"
