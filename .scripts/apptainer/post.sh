#!/usr/bin/env bash

# Stop On Error
set -eo pipefail

# Echo Commands
set -x

# Setup Environment and Fix Locale
dnf install -y gzip langpacks-en glibc-langpack-en glibc-locale-source glibc-common
localedef -i en_US -f UTF-8 en_US.UTF-8
export TZ="America/Denver"

# Update Packages
dnf upgrade -q -y

# Install EPEL
dnf install -q -y dnf-utils
dnf config-manager -q -y --set-enabled crb
dnf install -q -y epel-release

# Install System RPM Packages
dnf install -q -y --allowerasing \
	bash \
	bash-completion \
	colordiff \
	coreutils \
	diffutils \
	dos2unix \
	emacs-nox \
	fd-find \
	findutils \
	git-all \
	git-extras \
	gnupg2 \
	neovim \
	nodejs \
	npm \
	openssh-clients \
	openssl \
	pinentry-curses \
	pinentry-tty \
	pre-commit \
	python3 \
	python3-gnupg \
	python3-pip \
	python3-setuptools \
	python3-wheel \
	rbenv \
	ripgrep \
	ruby \
	ruby-build-rbenv \
	ruby-devel \
	rubygem-bundler \
	rubygem-json \
	rubygem-open4 \
	rubygems \
	screen \
	ShellCheck \
	tmux \
	tree \
	vim-enhanced \
	wget \
	yamllint

# Install Yarn incase we need rails
wget -O /etc/yum.repos.d/yarn.repo "https://dl.yarnpkg.com/rpm/yarn.repo"
dnf install -q -y yarn

# Modify Global SSH Client
cat <<- EOF > /etc/ssh/ssh_config.d/99-apptainer.conf
	TCPKeepAlive yes
	ServerAliveInterval 15
	ServerAliveCountMax 20
	AddKeysToAgent yes
EOF
chmod '0644' /etc/ssh/ssh_config.d/99-apptainer.conf

# Install Code Server
curl -fsSL https://code-server.dev/install.sh | sh

# Cleanup
dnf clean all
rm -rf /var/cache/dnf
