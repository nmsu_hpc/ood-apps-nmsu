#!/usr/bin/env bash

# Stop On Error
set -e
set -o pipefail

# Ensure Working Directory
if [ "$(pwd)" != "$PRJ_DIR" ]; then
	cd "$PRJ_DIR" || exit 15
fi

# Ensure Pre-Commit Is Installed
pre-commit install --install-hooks

# Run Pre Commit
if [ "$1" == "all" ]; then
	git add --all
	pre-commit run --all-files
elif [ "$1" == "ci" ]; then
	# Fetching only up to the closest merge-base to save time
	git fetch --depth 1 origin "$CI_MERGE_REQUEST_DIFF_BASE_SHA"

	# Diff only changed files within MR
	readarray -t array < <(git diff-tree --name-only -r "$CI_MERGE_REQUEST_DIFF_BASE_SHA" "$CI_COMMIT_SHA")

	pre-commit run --files "${array[@]}"
elif [ "$1" == "branch" ]; then
	# Update  Origin main/master branch
	git fetch origin master

	# Git Current Working Local Branch
	branch="$(git symbolic-ref --short -q HEAD)"

	# Diff only changed files within branch
	readarray -t array < <(git diff --name-only "$branch" "$(git merge-base "$branch" "origin/master")")

	pre-commit run --files "${array[@]}"
elif [ "$1" == "current" ]; then
	git add --all
	pre-commit run

else

	echo ""
	echo "'project lint <command>'"
	echo ""
	echo "Commands"
	echo "--------"
	echo "all        - Lint all files in repository"
	echo "branch     - Lint all committed changes in branch"
	echo "current    - Lint uncommitted changes"
	echo ""
fi
