#!/usr/bin/env bash

# Stop On Error
set -e
set -o pipefail

# Create Colors
#color_red='\033[0;31m'
color_green='\033[0;32m'
color_reset='\033[0m'

# Tasks defined in arg1/$1

if [ "$1" == "cleanup" ]; then
	git checkout main
	git fetch --prune
	git pull --autostash

	# Remove Merged Branches
	git delete-merged-branches

	# Remove Squashed+Merged Branches
	git delete-squashed-branches main

elif [ "$1" == "pushnew" ]; then
	git push --set-upstream origin "$(git branch  --no-color  | grep -E '^\*' | awk '{print $2}')"

elif [ "$1" == "userlist" ]; then
	echo
	echo
	echo "Please review your git user configuration!"
	echo
	echo "User (Global)=$(git config --global user.name)"
	echo "Email (Global)=$(git config --global user.email)"
	echo
	echo "User (Local)=$(git config --local user.name)"
	echo "Email (Local)=$(git config --local user.email)"

elif [ "$1" == "userset" ]; then
	read -erp "Git User: " name
	read -erp "Git Email: " email
	echo

	flag=""
	prompt="Please select a configuration realm:"
	PS3="$prompt "
	select opt in local global; do
		if (( REPLY > 0 && REPLY <= 2 )) ; then
			flag="--$opt"
			break
		else
			echo "Invalid option. Try another one." >&2
		fi
	done

	git config $flag user.name "$name"
	git config $flag user.email "$email"

	echo "Git user information for the local repo is set."
	echo "User ($flag)=$(git config $flag user.name)"
	echo "Email ($flag)=$(git config $flag user.email)"

elif [ "$1" == "merge" ]; then
	branch="$(git rev-parse --abbrev-ref HEAD | sed 's|/|%2F|g')"
	echo ""
	echo "Review Existing Merge Requests:"
	echo -e "${color_green}https://gitlab.nmsu.edu/systems/ansible-ict-systems-monolithic/-/merge_requests${color_reset}"
	echo ""
	echo "New Merge Request:"
	echo -e "${color_green}https://gitlab.nmsu.edu/systems/ansible-ict-systems-monolithic/-/merge_requests/new?merge_request%5Bsource_branch%5D=${branch}${color_reset}"
	echo ""
else

	echo ""
	echo "'project git <command>'"
	echo ""
	echo "Commands"
	echo "--------"
	echo "cleanup    - Cleanup Merged+Deleted Branches"
	echo "pushnew    - Push New Branch to Remote"
	echo "userlist   - Show Git User Info (name/email)"
	echo "userset    - Set Git User Info (name/email)"
	echo "merge      - Print URL For New or Existing Merge Requests"
	echo ""

fi
