#!/usr/bin/env bash

# Create project program
function project() {

	# Create Colors
	local color_red
	color_red='\033[0;31m'
	local color_reset
	color_reset='\033[0m'
	local exm
	exm='!'

	# Verify Working Directory
	if [ -z "$PRJ_DIR" ]; then
		echo -e "${color_red}Env Var 'PRJ_DIR' not set${exm} Stopping${exm}${exm}${color_reset}"
		return 1
	elif ! [ "$(pwd)" == "$PRJ_DIR" ]; then
		echo -e "${color_red}The current working directory must equal '${PRJ_DIR}'${exm} Stopping${exm}${exm}${color_reset}"
		return 2
	fi

	# Ensure Bash Completions are Setup
	if ! complete -p project &> /dev/null; then
		complete -F _project project
	fi

	# Handle Arguments
	local cmd
	cmd="$(echo "$1" | tr '[:upper:]' '[:lower:]')"
	local sub
	sub="$(echo "$2" | tr '[:upper:]' '[:lower:]')"

	if [ "$cmd" == "git" ]; then
		bash .scripts/project/git.sh "$sub"
	elif [ "$cmd" == "lint" ]; then
		bash .scripts/project/lint.sh "$sub"
	elif [ "$cmd" == "commands" ]; then
		if [ "$sub" == "git" ]; then
			echo "cleanup pushnew userlist userset merge help"
		elif [ "$sub" == "lint" ]; then
			echo "all branch current help"
		else
			echo "git lint help"
		fi
	else
		echo ""
		echo "'project <command>'"
		echo "'project <command> <subcommand>'"
		echo ""
		echo "Commands"
		echo "--------"
		echo "git       - Additional commands that leverage Git"
		echo "lint      - Run pre-commit validation (ansible-lint)"
		echo ""
	fi
}
export -f project

# Create Bash Completions
## https://spin.atomicobject.com/2016/02/14/bash-programmable-completion/
# shellcheck disable=SC1000-SC9999
_project(){
	COMPREPLY=();
	local word="${COMP_WORDS[COMP_CWORD]}";
	if [ "$COMP_CWORD" -eq 1 ]; then
		COMPREPLY=($(compgen -W "$(project commands)" -- "$word"));
	else
		local words=("${COMP_WORDS[@]}");
		unset words[0];
		unset words[$COMP_CWORD];
		local completions=$(project commands "${words[@]}");
		COMPREPLY=($(compgen -W "$completions" -- "$word"));
	fi
}
export -f _project
complete -F _project project

# Ensure Pre-Commit Is Installed
pre-commit install --install-hooks &> /dev/null
