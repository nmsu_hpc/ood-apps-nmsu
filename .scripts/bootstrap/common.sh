#!/usr/bin/env bash

# Set Vars
PRJ_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" | awk 'NF{NF-=2}1' FS='/' OFS='/')"
CACHE_DIR="${PRJ_DIR}/cache"
SCRIPTS_DIR="${PRJ_DIR}/.scripts"
export PRJ_DIR CACHE_DIR SCRIPTS_DIR

# Ensure Cache
if [ ! -d "$CACHE_DIR" ]; then
	mkdir -p "$CACHE_DIR"
fi

# Create Colors
COLOR_RED='\033[0;31m'
COLOR_GREEN='\033[0;32m'
COLOR_RESET='\033[0m'
export COLOR_RED COLOR_GREEN COLOR_RESET

function EPHEMERAL_PORT() {
	low_bound=49152
	range=16384
	while true; do
		candidate=$(( low_bound + (RANDOM % range) ))
		if ! (echo "" >/dev/tcp/127.0.0.1/${candidate}) &>/dev/null ; then
			echo $candidate
			break
		fi
	done
}
export -f EPHEMERAL_PORT

function SETUP_CONTAINER() {
	if [ -z "$APPTAINER_COMMAND" ]; then
		# Verify Apptainer Is Installed
		if ! command -v apptainer &>/dev/null; then
			echo "'apptainer' program is missing! Stopping!!"
			exit 1
		fi

		# Select / Build Container
		SIF="$CACHE_DIR/container.sif"
		export SIF

		hash_file="$CACHE_DIR/container.md5sum"

		if [ ! -f "$hash_file" ] \
			|| ! md5sum -c "$hash_file" --status \
			|| [ ! -f "$SIF" ]
		then
			# Build Container
			echo -e "Building/Rebuilding Project Container!"
			apptainer build --force "$SIF" "$SCRIPTS_DIR/apptainer/container.def"

			# Update Checksum File
			{
				md5sum "$SIF"
				md5sum "$SCRIPTS_DIR/apptainer/container.def"
				md5sum "$SCRIPTS_DIR/apptainer/post.sh"
				md5sum "$SCRIPTS_DIR/apptainer/prj_comps.sh"
				md5sum "$SCRIPTS_DIR/apptainer/runscript.sh"
			} > "$hash_file"
		fi

		# Setup ENV Vars
		APPTAINERENV_PRJ_DIR="$PRJ_DIR"
		export APPTAINERENV_PRJ_DIR
		APPTAINERENV_CACHE_DIR="$CACHE_DIR"
		export APPTAINERENV_CACHE_DIR
		APPTAINERENV_SCRIPTS_DIR="$SCRIPTS_DIR"
		export APPTAINERENV_SCRIPTS_DIR

		# Fix Problem Bad ENV Vars
		unset DBUS_SESSION_BUS_ADDRESS
	fi
}
export -f SETUP_CONTAINER

function PRJ_EXEC () {
	if [ -n "$APPTAINER_COMMAND" ] \
		|| [ -d '/.singularity.d' ] \
		|| [ -d '/.apptainer.d' ]
	then
		# In Container Already
		# Run Command
		exec "$@"
	else
		# Run Container
		SETUP_CONTAINER
		apptainer exec \
			--bind="$XDG_RUNTIME_DIR,$PRJ_DIR" \
			"$SIF" "$@"
	fi
}
export -f PRJ_EXEC

function PRJ_RUN () {
	if [ -n "$APPTAINER_COMMAND" ] \
		|| [ -d '/.singularity.d' ] \
		|| [ -d '/.apptainer.d' ]
	then
		# In Container Already
		# Run Command
		exec "$@"
	else
		# Run Container
		SETUP_CONTAINER
		apptainer run \
			--bind="$XDG_RUNTIME_DIR,$PRJ_DIR" \
			"$SIF" "$@"
	fi
}
export -f PRJ_RUN
