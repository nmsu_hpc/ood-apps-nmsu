#!/usr/bin/env bash

# Stop on Error
set -eo pipefail

# Get Script Root
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Source Common Bash Script
source "${SCRIPT_DIR}/common.sh"

# Check User ID
if [ $EUID -ne 0 ]; then
	echo "'./bootstrap link-prod' must be run as the 'root' user! Stopping!!"
	exit 21
fi

# Ensure Permissions on PRJ_DIR for Global Access
chmod -R 'a+rX' "$PRJ_DIR"

# Get Apps List
APPS="$(tr '\n' ' ' < "$PRJ_DIR/apps.txt")"

# Set Working Directory
if [ -d "/var/www/ood/apps/sys" ]; then
	cd "/var/www/ood/apps/sys"
else
	echo "OnDemand System App Directory Missing! Stopping!!"
	exit 22
fi

for APP in $APPS; do
	echo "Updating $APP"

	# Test For Existing App
	if [ -d "./nmsu-$APP" ] && [ ! -L "./nmsu-$APP" ]; then
		echo "'nmsu-$APP' already exists standalone! Skipping!!"
		continue
	fi

	# Cleanup Previous Symlink
	if [ -L "./nmsu-$APP" ]; then
		rm -f "./nmsu-$APP"
	fi

	ln -s "$PRJ_DIR/$APP" "./nmsu-$APP"
done
