#!/usr/bin/env bash

# Stop on Error
set -eo pipefail

# Get Script Root
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Source Common Bash Script
source "${SCRIPT_DIR}/common.sh"

# Set VSCODE Env
PROJECT_VSCODE_ENV="TRUE"
APPTAINERENV_PROJECT_VSCODE_ENV="TRUE"
export PROJECT_VSCODE_ENV APPTAINERENV_PROJECT_VSCODE_ENV

# Setup code-server Directory
data_dir="$CACHE_DIR/code-server"
mkdir -p "$data_dir/extensions"

# Set Config File
code_server_config="$data_dir/config.yaml"

# Create Config File If It Doesn't Exist
if  [ ! -f "$code_server_config" ]; then
	# Set Bind Address With Free Port
	function EPHEMERAL_PORT() {
		low_bound=49152
		range=16384
		while true; do
			candidate=$(( low_bound + (RANDOM % range) ))
			if ! (echo "" >/dev/tcp/127.0.0.1/${candidate}) &>/dev/null ; then
				echo $candidate
				break
			fi
		done
	}
	free_port="$(EPHEMERAL_PORT)"
	code_bind_addr="127.0.0.1:${free_port}"

	# Generate Password
	code_password="$(tr -dc A-Za-z0-9 </dev/urandom | head -c 25 || true)"

	# Create Config File
	cat <<-EOF > "$code_server_config"
		auth: password
		password: $code_password
		bind-addr: $code_bind_addr
		cert: false
		user-data-dir: $data_dir
		extensions-dir: $data_dir/extensions
		disable-telemetry: true
		disable-update-check: true
	EOF
fi

# Get Info From Config File
pw="$(grep '^password' "$code_server_config" | awk -F':' '{print $2}' | tr -d '[:space:]')"
port="$(grep '^bind-addr' "$code_server_config" | awk -F':' '{print $3}' | tr -d '[:space:]')"

# Print Help Text
echo ""
echo "Access Server Via:"
echo -e "${COLOR_GREEN}http://localhost:${port}/${COLOR_RESET}"
echo -e "Password = ${COLOR_RED}${pw}${COLOR_RESET}"
echo ""
echo "See Log File at '$data_dir/server.log'."
echo "See Config File at '$code_server_config'."
echo ""
echo "Press Control+C To Stop Server..."
echo ""

# Start Server
PRJ_RUN code-server --config="$code_server_config" \
	--ignore-last-opened \
	"$PRJ_DIR" &> "$data_dir/server.log"
