#!/usr/bin/env bash

# Stop on Error
set -eo pipefail

# Get Script Root
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Source Common Bash Script
source "${SCRIPT_DIR}/common.sh"

# Get Apps List
APPS="$(tr '\n' ' ' < "$PRJ_DIR/apps-dev.txt")"

# Set Working Directory
if [ ! -d "$HOME/ondemand/dev" ]; then
	mkdir -p "$HOME/ondemand/dev"
fi
cd "$HOME/ondemand/dev"

for APP in $APPS; do
	echo "Updating $APP"

	# Test For Existing App
	if [ -d "./nmsu-$APP" ] && [ ! -L "./nmsu-$APP" ]; then
		echo "'nmsu-$APP' already exists standalone! Skipping!!"
		continue
	fi

	# Cleanup Previous Symlink
	if [ -L "./nmsu-$APP" ]; then
		rm -f "./nmsu-$APP"
	fi

	ln -s "$PRJ_DIR/$APP" "./nmsu-$APP"
done
