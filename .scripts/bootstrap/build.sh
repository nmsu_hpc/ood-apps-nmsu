#!/usr/bin/env bash

# Stop on Error
set -eo pipefail

# Get Script Root
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

# Source Common Bash Script
source "${SCRIPT_DIR}/common.sh"

# Build Container and Run Bundle Installer
PRJ_EXEC bundle install

# Install Git Hooks
PRJ_EXEC pre-commit install --install-hooks

# Verify Apptainer Is Installed
PRJ_EXEC bash -c "echo 'container is built!'"
