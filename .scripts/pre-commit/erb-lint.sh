#!/usr/bin/env bash

# Stop on Error
set -eo pipefail

if [ ! -d "./cache/bundle" ]; then
	bundle install
fi

bundle exec erblint "$@"
