# Batch Connect - Jupyter Lab / Notebook

This is an Open Ondemand app designed to run Jupyter Lab or Notebook. While this is primarily designed for Python environments, other kernels can be setup by hand, or included by default inside any provided containers.

An example, of a non-python Jupyter use case, would be a MATLAB container that includes both Jupyter Lab and the MATLAB Proxy. Jupyter can run a MATLAB kernal and even launch the Proxy web app.

Another example, of a non-python Jupyter use case, would be a R container with Jupyter Lab installed. This allows the use of the R kernel for notebooks. Though for R environments [RStudio](../rstudio/README.md) is likely preferred, but more complicated to setup. R and the R kernel can also be installed using conda environments instead of a container.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [Apptainer](https://apptainer.org/)
- [Jupyter](https://jupyter.org/)
  - This can be containerized or provided via Lmod modules.
    - For modules, a recommendation would be to use a base conda installation with Jupyter Lab and [nb_conda_kernels](https://anaconda.org/conda-forge/nb_conda_kernels) installed. This will auto discover Jupyter kernels in other conda environments (user installed or included in the conda installation).

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
  - `version:` module entries.
    - It's expected that the module will provide a `$SIF` environment variable.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `template/sif.sh`
  - Calls to apptainer assumes that the nodes apptainer.conf will specify any cluster specific mounts such as home, scratch, and project directories.
