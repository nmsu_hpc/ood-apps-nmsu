#!/usr/bin/env bash

# Setup Jupyter Notebook or Lab
if [ "$OOD_JINTERFACE" == "notebook" ]; then
	JUPYTER_CMD="jupyter notebook"
	CHECK_CMD="command -v jupyter &>/dev/null"
	JUPYTER_WARNING="Error: 'jupyter' is missing from the PATH inside the provided container! Stopping!!"
else
	JUPYTER_CMD="jupyter-lab"
	CHECK_CMD="command -v jupyter-lab &>/dev/null"
	JUPYTER_WARNING="Error: 'jupyter-lab' is missing from the PATH inside the provided container! Stopping!!"
fi

# Setup Package Manager (Conda or Pixi)
if [[ "$OOD_PACKAGE_MANAGER" == "conda" ]]; then
	PACKAGE_CMD=""
else
	PACKAGE_CMD="pixi run"
	CHECK_CMD="command -v pixi &>/dev/null"
	JUPYTER_WARNING="Error: 'pixi' is missing from the PATH inside the provided container! Stopping!!"
fi

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$OOD_SIF"
echo ""

# Full Command to Run Jupyter Notebook/Lab
FULL_CMD="$PACKAGE_CMD $JUPYTER_CMD"

ood_jp () {
	if [ -z "$OOD_SIF" ]; then
		echo 'Error: ENV "OOD_SIF" is not set! Stopping!!'
		exit 15
	elif [ ! -f "$OOD_SIF" ]; then
		echo "Error: '$OOD_SIF' does not exist! Stopping!!"
		exit 16
	elif ! apptainer exec "$OOD_SIF" bash -c "$CHECK_CMD"; then
		echo "$JUPYTER_WARNING"
		exit 17
	elif [ -n "$CUDA_VISIBLE_DEVICES" ]; then
		apptainer exec --nv "$OOD_SIF" $FULL_CMD "$@"
	else
		apptainer exec "$OOD_SIF" $FULL_CMD "$@"
	fi
}
export -f ood_jp
