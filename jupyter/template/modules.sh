#!/usr/bin/env bash

# shellcheck disable=SC2086


# Setup Jupyter Notebook or Lab
if [ "$OOD_JINTERFACE" == "notebook" ]; then
	JUPYTER_CMD="jupyter notebook"
	CHECK_CMD="command -v jupyter &>/dev/null"
	JUPYTER_WARNING="'jupyter' not in path! Stopping!!"
else
	JUPYTER_CMD="jupyter-lab"
	CHECK_CMD="command -v jupyter-lab &>/dev/null"
	JUPYTER_WARNING="'jupyter-lab' not in path! Stopping!!"
fi

# Setup Package Manager (Conda or Pixi)
if [[ "$OOD_PACKAGE_MANAGER" == "conda" ]]; then
	module load $OOD_MODULES
	PACKAGE_CMD=""
else
	module load pixi
	PACKAGE_CMD="pixi run"
	CHECK_CMD="command -v pixi &>/dev/null"
	JUPYTER_WARNING="'pixi' not in path! Stopping!!"
fi

# Report Modules
echo ""
echo "The Follow Modules Are Loaded:"
module --terse list
echo ""

# Full Command to Run Jupyter Notebook/Lab
FULL_CMD="$PACKAGE_CMD $JUPYTER_CMD"

# Setup Command Replacements
ood_jp () {
	if eval "$CHECK_CMD"; then
		$FULL_CMD "$@"
	else
		echo "$JUPYTER_WARNING"
		exit 22
	fi
}
export -f ood_jp
