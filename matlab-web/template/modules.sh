#!/usr/bin/env bash

# Load Initial Modules
# shellcheck disable=SC2086
module load $OOD_MODULES

# Report Modules
echo ""
echo "The Follow Modules Are Loaded:"
module --terse list
echo ""

# Validate Matlab is in PATH or Function
if ! which matlab &>/dev/null; then
	"Error: 'matlab' is not in PATH! Stopping!!"
	exit 8
fi

# Validate Matlab Proxy is in PATH or Function
if ! which matlab-proxy-app &>/dev/null; then
	"Error: 'matlab-proxy-app' is not in PATH! Stopping!!"
	exit 9
fi
