#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# Prepare 'code-server' based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/sif.sh"
else
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/modules.sh"
fi

# Modify  Proxy Envs
unset MWI_BASE_URL
MWI_APP_HOST="0.0.0.0"
MWI_APP_PORT="$PORT"
MWI_ENABLE_TOKEN_AUTH="$PASSWORD"
MLM_LICENSE_FILE="27000@twister.nmsu.edu"
export MWI_APP_HOST
export MWI_APP_PORT
export MWI_ENABLE_TOKEN_AUTH
export MLM_LICENSE_FILE

# Set working directory
cd "$OOD_WD" || true

# Run Matlab
echo -e "\nStarting Matlab Proxy App:"
matlab-proxy-app
