#!/usr/bin/env bash

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$OOD_SIF"
echo ""

# Setup Command Replacements
matlab-proxy-app () {
	if [ -z "$OOD_SIF" ]; then
		echo 'Error: ENV "SIF" is not set! Stopping!!'
		exit 15
	elif [ ! -f "$OOD_SIF" ]; then
		echo "Error: '$OOD_SIF' does not exist! Stopping!!"
		exit 16
	elif ! apptainer exec "$OOD_SIF" bash -c "command -v matlab &>/dev/null"; then
			echo "Error: 'matlab' is missing from the PATH inside the provided container! Stopping!!"
			exit 17
	elif ! apptainer exec "$OOD_SIF" bash -c "command -v matlab-proxy-app &>/dev/null"; then
			echo "Error: 'matlab-proxy-app' is missing from the PATH inside the provided container! Stopping!!"
			exit 18
	elif [ -n "$CUDA_VISIBLE_DEVICES" ]; then
		apptainer exec --nv "$OOD_SIF" matlab-proxy-app "$@"
	else
		apptainer exec "$OOD_SIF" matlab-proxy-app "$@"
	fi
}
export -f matlab-proxy-app
