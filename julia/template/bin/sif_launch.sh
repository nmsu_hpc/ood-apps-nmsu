#!/usr/bin/env bash

sif_launch () {
	# Validate SIF File
	if [ -z "$SIF" ] || [ ! -f "$SIF" ]; then
		script_root="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
		if [ -f "${script_root}/julia.sif" ]; then
			export SIF="${script_root}/julia.sif"
		else
			echo "'SIF' Environment Variable is not set and 'julia.sif' cannot be found!"
			echo "Stopping!"
			exit 2
		fi
		unset script_root
	fi

	# Validate XDG_RUNTIME_DIR
	if [ -n "$XDG_RUNTIME_DIR" ] && [ -d "$XDG_RUNTIME_DIR" ]; then
		export APPTAINER_BIND="$XDG_RUNTIME_DIR"
	else
		unset XDG_RUNTIME_DIR
	fi

	# Launch Command
	if [ -z "$CUDA_VISIBLE_DEVICES" ]; then
		apptainer exec "$SIF" "$@"
	else
		apptainer exec --nv "$SIF" "$@"
	fi
}
