# Batch Connect - Julia

This is an Open Ondemand app designed for the Julia Programming Language. Julia needs some special handling when it comes to Jupyter, and other user interfaces, which is why this is a seperate app. Supported user interfaces are Jupyter Lab/Notebook, Pluto.jl, and code-server.

## Required Software

Here are the basic software requirements:

- [OOD Ruby Initializers](../initializers/README.md)
- [Apptainer](https://apptainer.org/)
- [Julia](https://julialang.org/)
  - This can be containerized or provided via Lmod modules.
- [Jupyter](https://jupyter.org/)
  - This can be containerized or provided via Lmod modules.
    - Julia must be in the environment.
  - There is Julia library requirement, to provide a Julia Jupyter Kernel, but it must be installed in the users home directory. This app will handle checking and installing that library at runtime.
- [Pluto.jl](https://plutojl.org/)
  - Pluto is installed using the Julia package manager and it must be installed in the users home directory. This app will handle checking and installing Pluto at runtime. The only requirement is Julia itself.
- [Code-Server](https://github.com/coder/code-server)
  - This can be containerized or provided via Lmod modules.
    - Julia must be in the environment.

## Known Issues

- Julia module/container must not contain any global read-only package repositories/installs/environments.
  - This causes user based pkg commands (required by jupyter/pluto.jl) to fail with a read-only error. This error only exists if the Julia env has not yet been setup in the users home directory.  Local workaround is in the works.

## Site Customizations

To customize this app for other sites/institutions you will need to review the following:

- `form.yml.erb`
  - Value of `cluster:`
  - Any links to documentation.
  - `version:` module entries.
    - It's expected that the module will provide a `$SIF` environment variable.
- `shared/form.yml.erb`
  - Any links to documentation.
- `shared/partitions.json` and `shared/backfill.json`
  - Partition names and values.
- `manifest.yml`
  - links to documentation.
- `template/before.sh`
  - Update the host/HOST assignment to reflect your node names.  We use a '-ib0' to force the usage of specific network so this will break for most other people.
- `template/bin/sif_launch.sh`
  - Calls to apptainer assumes that the nodes apptainer.conf will specify any cluster specific mounts such as home, scratch, and project directories.

## Example Singularity Build File

```txt
BootStrap: docker
From: docker.io/rockylinux/rockylinux:8

%runscript
exec "$@"

%environment
export TZ="America/Denver"
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export JUPYTER_DATA_DIR="$HOME/.julia/jupyter-sif/1.9"

%post
# Set Mask
umask 0022

# Setup Environment and Fix Locale
dnf install -y langpacks-en glibc-langpack-en glibc-locale-source glibc-common
localedef --quiet -v -c -i en_US -f UTF-8 en_US.UTF-8 || if [ $? -ne 1 ]; then exit $?; fi
export TZ="America/Denver"
export CPATH=$CPATH:/usr/include/openmpi-x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

# Install Additional Repos
dnf install -y epel-release dnf-plugins-core
dnf config-manager --set-enabled powertools

# Update Packages
dnf upgrade -y

# Install Julia + Common Dependencies
dnf install -y -q redhat-lsb-core findutils wget git git-all git-extras ca-certificates gnupg2 pandoc qt5-qtbase-devel "@Development Tools"

# Setup Python 3.9
dnf install -y -q python39 python39-devel python39-pip python39-pyyaml python39-setuptools python39-test python39-toml python39-urllib3 python39-requests python39-cryptography python39-wheel chkconfig
alternatives --set python /usr/bin/python3.9
alternatives --set python3 /usr/bin/python3.9
update-alternatives --install /usr/bin/pip pip /usr/bin/pip3.9 50
update-alternatives --install /usr/bin/pip3 pip3 /usr/bin/pip3.9 50
alternatives --set pip /usr/bin/pip3.9
alternatives --set pip3 /usr/bin/pip3.9

# Install Code Server
curl -fsSL https://code-server.dev/install.sh | sh -s -- --version=4.11.0

# Install Jupyter Notebook/Lab
python3 -m pip install -q --upgrade pip wheel
python3 -m pip install -q --upgrade notebook jupyterlab jupyterlab-widgets jupyterlab-pygments ipykernel ipywidgets qtconsole pyqt5

# Install Julia
jver_full="1.9.2"
jver_short="1.9"
jpath="/opt/julia/$jver_short"

mkdir -p "$jpath"

curl -fL -o julia.tar.gz "https://julialang-s3.julialang.org/bin/linux/x64/${jver_short}/julia-${jver_full}-linux-x86_64.tar.gz"
tar -xzf julia.tar.gz -C "$jpath" --strip-components 1
/usr/bin/rm -f julia.tar.gz

ln -s "${jpath}/bin/julia" "/usr/local/bin/julia"

# Cleanup dnf Packages
dnf clean all
rm -rf /var/cache/dnf/*

%test
set -x
julia --version
code-server --version
jupyter-notebook --version
jupyter-lab --version
```
